'use strict';

import { Application } from "express";
import { Server } from "http";
import * as bodyParser from 'body-parser';
import axios from 'axios';
import { Order, parseOrder, orderMessage, Despatch, despatchAdvice } from "./templates";
const express = require('express');
const util = require('util');

const app: Application = express();

app.use(bodyParser.text({ type: "application/xml" }))
app.use(bodyParser.json());
app.use(express.static('static'));

const forwardAddress: string = process.env.FORWARD || "http://192.168.136.129:8080"
const scsnid: string = process.env.SCSNID || "urn:scsn:UNKNOWN"

let server: Server = app.listen(8081, function () {
    console.log('SCSN Seller started on 0.0.0.0:8081 (http) ');
});

server.setTimeout(120000);

let orders: Map<string, Order> = new Map();

app.use((req, res, next) => setTimeout(next, 100))

// SCSN API

app.use('/api/:version/order', (req, res) => {
    const order = parseOrder(req.body, req.params.version);
    console.log(`Received order ${order.id} on version ${req.params.version} (${order.version})`);
    order.status = 'Received'
    orders.set(order.id, order);
    res.send('');
})

// UI Interaction

app.get('/ui/orders', (req, res) => {
    res.send(Array.from(orders.values()));
})

app.post('/ui/orders/:orderId', (req, res) => {
    const order = orders.get(req.params.orderId);
    order.promisedDeliveryDate = new Date(req.body.promiseDate);
    let orderResponse = orderMessage(order)
    console.log(util.inspect(order, {depth: 10}));

    let headers = {};
    if (order.buyer.accessurl !== undefined) {
        headers = {
            'Forward-Recipient': order.buyer.scsnid,
            'Forward-To': order.buyer.idsid,
            'Forward-Accessurl': order.buyer.accessurl,
            'Forward-Sender': scsnid,
            'Content-Type': 'application/xml'
        }
    } else {
        headers = {
            'Forward-Id': order.buyer.scsnid,
            'Forward-Sender': scsnid,
            'Content-Type': 'application/xml'
        }
    }

    axios.post(`${forwardAddress}/${order.version}/orderresponse`, orderResponse, {
        headers: headers,
        timeout: 120000
    }).then(response => {
        order.status = 'Accepted';
        orders.set(order.id, order);
        res.send({
            status: "OK"
        });
    }).catch(error => {
        console.log(util.inspect(error, {depth: 20}));
        order.status = "Error";
        orders.set(order.id, order);
        res.status(500).send({
            status: "Error",
            message: error
        });
    });
});
app.post('/ui/orders/:orderId/despatchadvice', (req, res) => {
    const order = orders.get(req.params.orderId);
    let despatchadvice: Despatch = {
        id: `${Math.floor(Math.random() * 9000000) + 1000000}`,
        orderId: order.id,
        issueDate: order.issueDate,
        buyer: order.buyer,
        seller: order.seller,
        product: order.product
    }
    let headers = {};
    if (order.buyer.accessurl !== undefined) {
        headers = {
            'Forward-Recipient': order.buyer.scsnid,
            'Forward-To': order.buyer.idsid,
            'Forward-Accessurl': order.buyer.accessurl,
            'Forward-Sender': scsnid,
            'Content-Type': 'application/xml'
        }
    } else {
        headers = {
            'Forward-Id': order.buyer.scsnid,
            'Forward-Sender': scsnid,
            'Content-Type': 'application/xml'
        }
    }
    axios.post(`${forwardAddress}/${order.version}/despatchadvice`, despatchAdvice(despatchadvice), {
        headers: headers,
        timeout: 120000
    }).then(response => {
        order.status = 'Shipped';
        orders.set(order.id, order);
        res.send({
            status: "OK"
        });
    }).catch(error => {
        console.log(util.inspect(error, {depth: 20}));
        order.status = "Error";
        orders.set(order.id, order);
        res.status(500).send({
            status: "Error",
            message: error
        });
    });
});
app.post('/ui/orders/:orderId/invoice', (req, res) => {
    const order = orders.get(req.params.orderId);
    let headers = {};
    if (order.buyer.accessurl !== undefined) {
        headers = {
            'Forward-Recipient': order.buyer.scsnid,
            'Forward-To': order.buyer.idsid,
            'Forward-Accessurl': order.buyer.accessurl,
            'Forward-Sender': scsnid,
            'Content-Type': 'application/xml'
        }
    } else {
        headers = {
            'Forward-Id': order.buyer.scsnid,
            'Forward-Sender': scsnid,
            'Content-Type': 'application/xml'
        }
    }
    axios.post(`${forwardAddress}/${order.version}/invoice`, order.id, {
        headers: headers,
        timeout: 120000
    }).then(response => {
        order.status = 'Completed';
        orders.set(order.id, order);
        res.send({
            status: "OK"
        });
    }).catch(error => {
        console.log(util.inspect(error, {depth: 20}));
        order.status = "Error";
        orders.set(order.id, order);
        res.status(500).send({
            status: "Error",
            message: error
        });
    });
});

app.get('/ui/orders/:orderId/file/:filename', (req, res) => {
    const order = orders.get(req.params.orderId);
    let headers = {};
    if (order.seller.accessurl !== undefined) {
        headers = {
            'Forward-Recipient': order.buyer.scsnid,
            'Forward-To': order.buyer.idsid,
            'Forward-Accessurl': order.buyer.accessurl,
            'Forward-Sender': scsnid,
            'Content-Type': 'application/xml'
        }
    } else {
        headers = {
            'Forward-Id': order.buyer.scsnid,
            'Forward-Sender': scsnid,
            'Content-Type': 'application/xml'
        }
    }
    axios.get(`${forwardAddress}/${order.version}/file/${req.params.filename}`, {
        headers: headers,
        timeout: 120000
    }).then(response => {
        res.set({"Content-Disposition":`attachment; filename="aluminium"`});
        res.send(response.data)
    }).catch(error => {
        console.log(util.inspect(error, {depth: 20}));
        order.status = "Error";
        orders.set(order.id, order);
        res.status(500).send({
            status: "Error",
            message: error
        });
    });
});

process.on('SIGTERM', () => {
    server.close(() => {
        console.log('Process terminated');
    })
});