'use strict';

import { version } from "punycode";

const convert = require('xml-js');
const moment = require('moment');

export class Despatch {
    id: string
    orderId: string
    issueDate: Date
    buyer: Party
    seller: Party
    product: string
}

export class Order {
    id?: string
    status: string
    issueDate: Date
    requestedDeliveryDate: Date
    promisedDeliveryDate?: Date
    buyer: Party
    seller: Party
    accounting: Party
    product: string
    quantity: number
    version: string
}

export class Party {
    name: string
    idsid?: string
    accessurl?: string
    scsnid: string
    tin: string
    kvk: string
}

export const parseOrder: (xml: string, version: string) => Order = function(xml: string, version: string) {
    let parsedXml = convert.xml2js(xml, {compact: true});
    let type = (parsedXml['Order']) ? 'Order' : 'OrderResponse'; 
    let promisedDate = (type === 'OrderResponse') ? new Date(parsedXml[type]['cac:OrderLine']?.['cac:LineItem']?.['cac:Delivery']?.['cac:PromisedDeliveryPeriod']?.['cbc:EndDate']?.['_text']) : undefined
    let buyerAccessUrl = parsedXml[type]['cac:BuyerCustomerParty']?.['cac:Party']?.['cac:Contact']?.['cac:OtherCommunication']?.['cbc:Value']?.['_text']
    if (buyerAccessUrl?.trim() === '') {
        buyerAccessUrl = undefined
    }
    let sellerAccessUrl = parsedXml[type]['cac:SellerSupplierParty']?.['cac:Party']?.['cac:Contact']?.['cac:OtherCommunication']?.['cbc:Value']?.['_text']
    if (sellerAccessUrl?.trim() === '') {
        sellerAccessUrl = undefined
    }
    let buyerConnectorId = parsedXml[type]['cac:BuyerCustomerParty']?.['cac:Party']?.['cac:PhysicalLocation']?.['cbc:Description']?.['_text']
    if (buyerConnectorId?.trim() === '') {
        buyerConnectorId = undefined
    }
    let sellerConnectorId = parsedXml[type]['cac:SellerSupplierParty']?.['cac:Party']?.['cac:PhysicalLocation']?.['cbc:Description']?.['_text']
    if (sellerConnectorId?.trim() === '') {
        sellerConnectorId = undefined
    }
    console.log(`Buyer: ${buyerAccessUrl} ${buyerConnectorId}, seller: ${sellerAccessUrl} ${sellerConnectorId}`);
    return {
        id: parsedXml[type]['cbc:ID']?.['_text'],
        version: version,
        status: (type === 'OrderResponse') ? "Accepted" : "Placed",
        issueDate: new Date(parsedXml[type]['cbc:IssueDate']?.['_text']),
        requestedDeliveryDate: new Date(parsedXml[type]['cac:OrderLine']?.['cac:LineItem']?.['cac:Delivery']?.['cac:RequestedDeliveryPeriod']?.['cbc:EndDate']?.['_text']),
        promisedDeliveryDate: promisedDate,
        buyer: {
            name: parsedXml[type]['cac:BuyerCustomerParty']?.['cac:Party']?.['cac:PartyName']?.['cbc:Name']?.['_text'],
            scsnid: parsedXml[type]['cac:BuyerCustomerParty']?.['cac:Party']?.['cac:PartyIdentification']?.['cbc:ID']?.['_text'],
            tin: parsedXml[type]['cac:BuyerCustomerParty']?.['cac:Party']?.['cac:PartyTaxScheme']?.['cbc:CompanyID']?.['_text'],
            kvk: parsedXml[type]['cac:BuyerCustomerParty']?.['cac:Party']?.['cac:PartyLegalEntity']?.['cbc:CompanyID']?.['_text'],
            accessurl: buyerAccessUrl,
            idsid: buyerConnectorId
        },
        seller: {
            name: parsedXml[type]['cac:SellerSupplierParty']?.['cac:Party']?.['cac:PartyName']?.['cbc:Name']?.['_text'],
            scsnid: parsedXml[type]['cac:SellerSupplierParty']?.['cac:Party']?.['cac:PartyIdentification']?.['cbc:ID']?.['_text'],
            tin: parsedXml[type]['cac:SellerSupplierParty']?.['cac:Party']?.['cac:PartyTaxScheme']?.['cbc:CompanyID']?.['_text'],
            kvk: parsedXml[type]['cac:SellerSupplierParty']?.['cac:Party']?.['cac:PartyLegalEntity']?.['cbc:CompanyID']?.['_text'],
            accessurl: sellerAccessUrl,
            idsid: sellerConnectorId
        },
        accounting: {
            name: parsedXml[type]['cac:AccountingCustomerParty']?.['cac:Party']?.['cac:PartyName']?.['cbc:Name']?.['_text'],
            scsnid: parsedXml[type]['cac:AccountingCustomerParty']?.['cac:Party']?.['cac:PartyIdentification']?.['cbc:ID']?.['_text'],
            tin: parsedXml[type]['cac:AccountingCustomerParty']?.['cac:Party']?.['cac:PartyTaxScheme']?.['cbc:CompanyID']?.['_text'],
            kvk: parsedXml[type]['cac:AccountingCustomerParty']?.['cac:Party']?.['cac:PartyLegalEntity']?.['cbc:CompanyID']?.['_text'],
        },
        product: parsedXml[type]['cac:OrderLine']?.['cac:LineItem']?.['cac:Item']?.['cbc:Name']?.['_text'],
        quantity: parseInt(parsedXml[type]['cac:OrderLine']?.['cac:LineItem']?.['cbc:Quantity']?.['_text'])
    }
}

export const party: (party: Party) => string = (party: Party) => `<cac:Party>
    <cac:PartyIdentification>
        <cbc:ID schemeID="GLN">${party.scsnid}</cbc:ID>
    </cac:PartyIdentification>
    <cac:PartyName>
        <cbc:Name>${party.name}</cbc:Name>
    </cac:PartyName>
    <cac:PartyTaxScheme>
        <cbc:CompanyID>${party.tin}</cbc:CompanyID>
    <cac:TaxScheme/>
    </cac:PartyTaxScheme>
    <cac:PartyLegalEntity>
        <cbc:CompanyID schemeID="NL:KVK">${party.kvk}</cbc:CompanyID>
    </cac:PartyLegalEntity>
    <cac:PhysicalLocation>
        <cbc:Description>${party.idsid}</cbc:Description>
    </cac:PhysicalLocation>
    <cac:Contact>
        <cac:OtherCommunication>
            <cbc:Value>${party.accessurl}</cbc:Value>
        </cac:OtherCommunication>
    </cac:Contact>
</cac:Party>`

export const date = (date: Date) => `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`

export const orderMessage: (order: Order) => string = (order: Order) => `<?xml version="1.0" encoding="UTF-8"?>
<${(order.promisedDeliveryDate !== undefined) ? 'OrderResponse' : 'Order'} xmlns="urn:oasis:names:specification:ubl:schema:xsd:Order-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Order-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Order-2.1.xsd">
	<!-- SCSN Order example based on UBL Order version 2.1 -->
	<cbc:ID>${order.id}</cbc:ID>
    <cbc:IssueDate>${moment(order.issueDate).format('YYYY-MM-DD')}</cbc:IssueDate>
	<cac:BuyerCustomerParty>
        ${party(order.buyer)}
	</cac:BuyerCustomerParty>
	<cac:SellerSupplierParty>
        ${party(order.seller)}
	</cac:SellerSupplierParty>
	<cac:AccountingCustomerParty>
        ${party(order.accounting)}
	</cac:AccountingCustomerParty>
	<cac:PaymentTerms>
		<cbc:ID>124</cbc:ID>
		<cbc:Note>[Additional information on payment terms]</cbc:Note>
		<cbc:SettlementDiscountPercent>5</cbc:SettlementDiscountPercent>
		<cbc:SettlementDiscountAmount currencyID="EUR">100</cbc:SettlementDiscountAmount>
		<cac:SettlementPeriod>
			<cbc:DurationMeasure unitCode="Days">5.0</cbc:DurationMeasure>
		</cac:SettlementPeriod>
	</cac:PaymentTerms>
	<cac:TransactionConditions>
		<cbc:Description>Accept within 5 days</cbc:Description>
	</cac:TransactionConditions>
	<cac:AnticipatedMonetaryTotal>
		<cbc:PayableAmount currencyID="EUR">1000</cbc:PayableAmount>
	</cac:AnticipatedMonetaryTotal>
	<cac:OrderLine>
		<cac:LineItem>
			<cbc:ID>1</cbc:ID>
			<cbc:Note>[Free-form text note on orderline]</cbc:Note>
			<cbc:LineStatusCode>accepted</cbc:LineStatusCode>
			<cbc:Quantity unitCode="KGM">${order.quantity}</cbc:Quantity>
			<cbc:LineExtensionAmount currencyID="EUR">175</cbc:LineExtensionAmount>
			<cac:Delivery>
				<cbc:ID>${Math.floor(Math.random() * 9000000) + 1000000}</cbc:ID>
				<cbc:Quantity unitCode="KGM">50</cbc:Quantity>
				<cac:DeliveryLocation>
					<cbc:ID schemeID="GLN">${order.buyer.scsnid}</cbc:ID>
				</cac:DeliveryLocation>
				<cac:RequestedDeliveryPeriod>
					<cbc:EndDate>${moment(order.requestedDeliveryDate).format('YYYY-MM-DD')}</cbc:EndDate>
				</cac:RequestedDeliveryPeriod>
                ${(order.promisedDeliveryDate !== undefined) ? 
                    `<cac:PromisedDeliveryPeriod>
                    <cbc:EndDate>${moment(order.promisedDeliveryDate).format('YYYY-MM-DD')}</cbc:EndDate>
                </cac:PromisedDeliveryPeriod>` : ''}
			</cac:Delivery>
			<cac:Price>
				<cbc:PriceAmount currencyID="EUR">4</cbc:PriceAmount>
				<cbc:BaseQuantity unitCode="KGM">1</cbc:BaseQuantity>
				<cbc:PriceType>net</cbc:PriceType>
				<cac:AllowanceCharge>
					<cbc:ChargeIndicator>false</cbc:ChargeIndicator>
					<cbc:Amount currencyID="EUR">0.5</cbc:Amount>
				</cac:AllowanceCharge>
			</cac:Price>
            <cac:Item>
                <cbc:Description>Sheet of ${order.product}</cbc:Description>
                <cbc:Name>${order.product}</cbc:Name>
                <cac:BuyersItemIdentification>
                    <cbc:ID>${order.product}</cbc:ID>
                </cac:BuyersItemIdentification>
                <cac:SellersItemIdentification>
                    <cbc:ID>${order.product}-1</cbc:ID>
                    <cbc:ExtendedID>A1</cbc:ExtendedID>
                </cac:SellersItemIdentification>
                <cac:Dimension>
                    <cbc:AttributeID>Length</cbc:AttributeID>
                    <cbc:Measure unitCode="MMT">220</cbc:Measure>
                </cac:Dimension>
            </cac:Item>
		</cac:LineItem>
	</cac:OrderLine>
</${(order.promisedDeliveryDate !== undefined) ? 'OrderResponse' : 'Order'}>`

export const parseDespatchAdvice = (xml: string) => convert.xml2js(xml, {compact: true})['DespatchAdvice']?.['cac:OrderReference']?.['cbc:ID']?.['_text'];

export const despatchAdvice: (despatch: Despatch) => string = (despatch: Despatch) => `<?xml version="1.0" encoding="UTF-8"?>
<DespatchAdvice xmlns="urn:oasis:names:specification:ubl:schema:xsd:Order-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Order-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Order-2.1.xsd">
	<!-- SCSN Order example based on UBL Order version 2.1 -->
	<cbc:ID>${despatch.id}</cbc:ID>
    <cbc:IssueDate>${moment(despatch.issueDate).format('YYYY-MM-DD')}</cbc:IssueDate>
    <cac:OrderReference>
        <cbc:ID>${despatch.orderId}</cbc:ID>
    </cac:OrderReference>
	<cac:BuyerCustomerParty>
        ${party(despatch.buyer)}
	</cac:BuyerCustomerParty>
	<cac:SellerSupplierParty>
        ${party(despatch.seller)}
	</cac:SellerSupplierParty>
	<cac:DeliveryCustomerParty>
        ${party(despatch.buyer)}
	</cac:DeliveryCustomerParty>
	<cac:DespatchSupplierParty>
        ${party(despatch.seller)}
	</cac:DespatchSupplierParty>
	<cac:DespatchLine>
		<cac:LineItem>
			<cac:Item>
                <cbc:Description>Sheet of ${despatch.product}</cbc:Description>
                <cbc:Name>${despatch.product}</cbc:Name>
                <cac:BuyersItemIdentification>
                    <cbc:ID>${despatch.product}</cbc:ID>
                </cac:BuyersItemIdentification>
                <cac:SellersItemIdentification>
                    <cbc:ID>${despatch.product}-1</cbc:ID>
                    <cbc:ExtendedID>A1</cbc:ExtendedID>
                </cac:SellersItemIdentification>
                <cac:ItemInstance>
					<cbc:SerialID>1</cbc:SerialID>
					<cbc:SerialID>2</cbc:SerialID>
					<cbc:SerialID>3</cbc:SerialID>
					<cbc:SerialID>4</cbc:SerialID>
					<cbc:SerialID>5</cbc:SerialID>
					<cbc:SerialID>6</cbc:SerialID>
					<cbc:SerialID>7</cbc:SerialID>
				</cac:ItemInstance>
			</cac:Item>
		</cac:LineItem>
	</cac:DespatchLine>
</DespatchAdvice>`