document.addEventListener('DOMContentLoaded', async function (event) {
    const api = axios.create({
        baseURL: 'ui/',
        timeout: 120000
    });
    Vue.filter('formatDate', function(value) {
        if (value) {
          return moment(String(value)).format('MM/DD/YYYY')
        }
      }
    );
    const orders = new Vue({
        el: '#orders',
        data: {
            orders: [],
            promiseDates: {},
            busy: {}
        },
        created: async function () {
            this.fetchOrders();
            this.timer = setInterval(this.fetchOrders, 5 * 1000);
        },
        methods: {
            fetchOrders: async function () {
                ordersResponse = await api.get('orders');
                this.orders = ordersResponse.data;
                this.orders.forEach(order => {
                    if (this.promiseDates[order.id] === undefined) {
                        this.promiseDates[order.id] = moment(order.requestedDeliveryDate).format('YYYY-MM-DD');
                    }
                });
                document.getElementById('pageloader').classList.add("hidden");
            },
            promise: async function (orderId) {
                let order = this.orders.filter(order => order.id == orderId)[0]
                this.busy[orderId] = true;
                if (order === undefined) {
                    alert('Error');
                }
                api.post(`orders/${orderId}`, { promiseDate: this.promiseDates[orderId] })
                    .finally(() => {
                        this.busy[orderId] = false;
                    })
            },
            despatch: async function (orderId) {
                this.busy[orderId] = true;
                api.post(`orders/${orderId}/despatchadvice`)
                    .finally(() => {
                        this.busy[orderId] = false;
                    })
            },
            invoice: async function (orderId) {
                this.busy[orderId] = true;
                api.post(`orders/${orderId}/invoice`)
                    .finally(() => {
                        this.busy[orderId] = false;
                    })
            }
        },
        beforeDestroy: function () {
            clearInterval(this.timer);
        }
    });
});