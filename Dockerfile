FROM node:12-alpine as builder

WORKDIR /app

ADD package*.json /app/
RUN npm install

ADD . /app
RUN npm run tsc

FROM node:12-alpine

WORKDIR /app

COPY --from=builder /app/node_modules /app/node_modules
COPY --from=builder /app/dist/src /app

ADD static /app/static

EXPOSE 8081

ENTRYPOINT [ "node", "index.js" ]

HEALTHCHECK --interval=10s --timeout=3s --retries=3 \
  CMD wget --quiet --tries=1 --spider http://localhost:8081/ || exit 1